// Initialize font-sizes and baseline. Use pixels for font sizes, and percentages for baseline.
$baseline: 100%;
$default: 16px;
$header1: 67.34px;
$header2: 50.52px;
$header3: 37.90px;
$header4: 21.33px;
$header5: 21.33px;
$header6: 16px;
    
// Breakpoints for font calculation. Set the ceiling for each one (i.e., if mobile switches to tablet at 640px, then set $mobile to 640px. Again, use pixels.)
$mobile: 640px;
$tablet: 1024px;
$desktop: 1440px;
$hidpi: 2160px;

// The actual breakpoints for use in @media-queries
$tabletBreak: $mobile / $default + 0em;
$desktopBreak: $tablet / $default + 0em;
$hidpiBreak: $desktop / $default + 0em;
    
/* Let's do the math. */
/* To determine font size we do f = s / b * 100%, where s is the font size in pixels and b is the breakpoint in pixels */
/* Line height is the same formula, but at 125%. */

/* Default/Mobile */
body {
    font-size: $baseline;
    line-height: $baseline + 25%;
}

p {
    font-size: $default / $mobile * 100vw;
    line-height: $default / $mobile * 125vw;
}

h1 {
    font-size: $header1 / $mobile * 100vw;
    line-height: $header1 / $mobile * 125vw;
}

h2 {
    font-size: $header2 / $mobile * 100vw;
    line-height: $header2 / $mobile * 125vw;
}

h3 {
    font-size: $header3 / $mobile * 100vw;
    line-height: $header3 / $mobile * 125vw;
}

h4 {
    font-size: $header4 / $mobile * 100vw;
    line-height: $header4 / $mobile * 125vw;
}

h5 {
    font-size: $header5 / $mobile * 100vw;
    line-height: $header5 / $mobile * 125vw;
}

h6 {
    font-size: $header6 / $mobile * 100vw;
    line-height: $header6 / $mobile * 125vw;
}

ul, ol {
    li {
        font-size: $default / $mobile * 87.5vw;
        line-height: $default / $mobile * 112.5vw;
    }
}

small { font-size: 81.25%; }
    
    
/* Tablet Breakpoint */
@media screen and (min-width: $tabletBreak) {  
    p {
        font-size: $default / $tablet * 100vw;
        line-height: $default / $tablet * 125vw;
    }

    h1 {
        font-size: $header1 / $tablet * 100vw;
        line-height: $header1 / $tablet * 125vw;
    }

    h2 {
        font-size: $header2 / $tablet * 100vw;
        line-height: $header2 / $tablet * 125vw;
    }

    h3 {
        font-size: $header3 / $tablet * 100vw;
        line-height: $header3 / $tablet * 125vw;
    }

    h4 {
        font-size: $header4 / $tablet * 100vw;
        line-height: $header4 / $tablet * 125vw;
    }

    h5 {
        font-size: $header5 / $tablet * 100vw;
        line-height: $header5 / $tablet * 125vw;
    }

    h6 {
        font-size: $header6 / $tablet * 100vw;
        line-height: $header6 / $tablet * 125vw;
    }

    ul, ol {
        li {
            font-size: $default / $tablet * 87.5vw;
            line-height: $default / $tablet * 112.5vw;
        }
    }
}
    
    
/* Desktop Breakpoint */
@media screen and (min-width: $desktopBreak) {  
    p {
        font-size: $default / $desktop * 100vw;
        line-height: $default / $desktop * 125vw;
    }

    h1 {
        font-size: $header1 / $desktop * 100vw;
        line-height: $header1 / $desktop * 125vw;
    }

    h2 {
        font-size: $header2 / $desktop * 100vw;
        line-height: $header2 / $desktop * 125vw;
    }

    h3 {
        font-size: $header3 / $desktop * 100vw;
        line-height: $header3 / $desktop * 125vw;
    }

    h4 {
        font-size: $header4 / $desktop * 100vw;
        line-height: $header4 / $desktop * 125vw;
    }

    h5 {
        font-size: $header5 / $desktop * 100vw;
        line-height: $header5 / $desktop * 125vw;
    }

    h6 {
        font-size: $header6 / $desktop * 100vw;
        line-height: $header6 / $desktop * 125vw;
    }

    ul, ol {
        li {
            font-size: $default / $desktop * 87.5vw;
            line-height: $default / $desktop * 112.5vw;
        }
    }
}
    
    
/* HiDPI/Ultrawide Breakpoint */
@media screen and (min-width: $hidpiBreak) {  
    p {
        font-size: $default / $hidpi * 100vw;
        line-height: $default / $hidpi * 125vw;
    }

    h1 {
        font-size: $header1 / $hidpi * 100vw;
        line-height: $header1 / $hidpi * 125vw;
    }

    h2 {
        font-size: $header2 / $hidpi * 100vw;
        line-height: $header2 / $hidpi * 125vw;
    }

    h3 {
        font-size: $header3 / $hidpi * 100vw;
        line-height: $header3 / $hidpi * 125vw;
    }

    h4 {
        font-size: $header4 / $hidpi * 100vw;
        line-height: $header4 / $hidpi * 125vw;
    }

    h5 {
        font-size: $header5 / $hidpi * 100vw;
        line-height: $header5 / $hidpi * 125vw;
    }

    h6 {
        font-size: $header6 / $hidpi * 100vw;
        line-height: $header6 / $hidpi * 125vw;
    }

    ul, ol {
        li {
            font-size: $default / $hidpi * 87.5vw;
            line-height: $default / $hidpi * 112.5vw;
        }
    }
}
    

    
